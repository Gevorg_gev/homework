﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Serialization_1
{
    public class Employee
    {
        private int _Id;
        public int Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        private string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        private string _Surname;
        public string Surname
        {
            get { return _Surname; }
            set { _Surname = value; } 
        }

        public Employee(int Id, string Name, string Surname)
        {
            this.Id = Id;
            this.Name = Name;
            this.Surname = Surname;
        }
    }
}
